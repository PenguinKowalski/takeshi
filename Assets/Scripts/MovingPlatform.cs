﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform transA, transB;

    Rigidbody rb;
    public float t = 0;
    public float speed = 1;
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        t=  Mathf.PingPong(Time.time*speed, 1);

        Vector3 newPosition = Vector3.Lerp(transA.position, transB.position, t);
        transform.position= newPosition;

    }
}
