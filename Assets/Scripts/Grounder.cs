﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Grounder : MonoBehaviour
{
    // Start is called before the first frame update
    public LayerMask layers;
    public float distance;
    RaycastHit hit;

    public Transform icona_ground, armature;

    void Update()
    {

        Debug.DrawRay(armature.position+ Vector3.up*0.5f, Vector3.down*distance, Color.green);
        if (Physics.Raycast(armature.position + Vector3.up * 0.5f, Vector3.down,  out hit, distance, layers))
        {
            this.transform.SetParent(hit.collider.transform);
            icona_ground.gameObject.SetActive(true);
            icona_ground.position = hit.point + Vector3.up * 0.01f;


        }   
        else
        {
            icona_ground.gameObject.SetActive(false);

        }
    }

    

}
