﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Respawner : MonoBehaviour
{
    // Start is called before the first frame update
    public LayerMask layers;
    public float distance;
    RaycastHit hit;
    float t = 0;
    float timerCaduta = 0;
    public Transform armature;
    public float tempoCaduta=2f;
    public Vector3 lastPosition;

    Rigidbody rb;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }
    void Update()
    {
        t += Time.deltaTime;

        bool updatePosition=false;
        if (t > 1)
        {
            updatePosition = true; 

            t = 0;
        }

        Debug.DrawRay(armature.position+ Vector3.up*0.5f, Vector3.down*distance, Color.green);
        if (Physics.Raycast(armature.position + Vector3.up * 0.5f, Vector3.down,  out hit, distance, layers))
        {
            timerCaduta = 0;

            if (updatePosition)
            {
                if (!hit.collider.CompareTag("Unsafe"))
                {
                    lastPosition = this.transform.position;
                }
            }
        }   
        else
        {

            timerCaduta += Time.deltaTime;
            if (timerCaduta > tempoCaduta)
            {
                Caduta();
                timerCaduta = 0;
            }

        }
    }

    

    void Caduta()
    {
        this.transform.position = lastPosition + Vector3.up;
        rb.velocity = Vector3.zero;

    }
}
