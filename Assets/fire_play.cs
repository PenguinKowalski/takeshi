﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fire_play : MonoBehaviour
{
    public ParticleSystem _fire;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(play_fire());
    }

    IEnumerator play_fire()
    {
        while (true)
        {
            _fire.Play();
            yield return new WaitForSeconds(7f);
        }
    }

}
