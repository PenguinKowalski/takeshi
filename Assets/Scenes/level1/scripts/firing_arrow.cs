﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firing_arrow : MonoBehaviour
{
    public GameObject arrow;

    public float arrow_speed = 50f;

    void Start()
    {
        StartCoroutine(fire_arrow());
    }

    IEnumerator fire_arrow()
    {
        while (true)
        {
            spawn_fire_arrow();
            yield return new WaitForSeconds(1f);
        }
    }

    public void spawn_fire_arrow()
    {
        GameObject a = Instantiate(arrow) as GameObject;
        a.transform.position = this.transform.position;
        a.transform.rotation = this.transform.rotation;
        Rigidbody a_rb = a.GetComponent<Rigidbody>();
        a_rb.AddForce(Vector3.left * arrow_speed);
    }
}
